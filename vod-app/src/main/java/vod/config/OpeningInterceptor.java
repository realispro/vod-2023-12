package vod.config;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.HandlerInterceptor;

import java.time.LocalTime;

public class OpeningInterceptor implements HandlerInterceptor {

    private int opening = 9;
    private int closing = 17;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        int currentHour = LocalTime.now().getHour();

        if(currentHour>=opening && currentHour <closing){
            return true;
        } else {
            response.setStatus(503);
            return false;
        }
    }
}
