package vod.repository.jpa;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import vod.model.Cinema;
import vod.model.Movie;
import vod.repository.CinemaDao;

import java.util.List;
import java.util.Optional;

@Repository
@Primary
@RequiredArgsConstructor
@Slf4j
public abstract class JpaCinemaDao implements CinemaDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Cinema> findAll() {
        // SQL -> HQL -> JPQL
        return entityManager.createQuery("select c from Cinema c", Cinema.class).getResultList();
    }

    @Override
    public Optional<Cinema> findById(Integer id) {
        return Optional.ofNullable(
                entityManager.find(Cinema.class, id)
        );
    }

    @Override
    public List<Cinema> findByMovie(Movie m) {
        return entityManager.createQuery("select c from Cinema c join c.movies movie where movie=:movie", Cinema.class)
                .setParameter("movie", m)
                .getResultList();
    }

    @Override
    public Cinema save(Cinema c) {
        entityManager.persist(c);
        return c;
    }
}
