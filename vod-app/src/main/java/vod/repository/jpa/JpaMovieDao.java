package vod.repository.jpa;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Transient;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vod.model.Cinema;
import vod.model.Director;
import vod.model.Movie;
import vod.repository.CinemaDao;
import vod.repository.MovieDao;

@Repository
@Primary
@RequiredArgsConstructor
@Slf4j
public abstract class JpaMovieDao implements MovieDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Movie> findAll() {
        return entityManager.createQuery("select m from Movie m", Movie.class).getResultList();
    }

    @Override
    public Optional<Movie> findById(Integer id) {
        return Optional.ofNullable(entityManager.find(Movie.class, id));
    }

    @Override
    public List<Movie> findByDirector(Director d) {
        return entityManager
                .createQuery("select m from Movie m where m.director=:director", Movie.class)
                .setParameter("director", d)
                .getResultList();
    }

    @Override
    public List<Movie> findByCinema(Cinema m) {
        return entityManager
                .createQuery(
                        "select m from Movie m join m.cinemas cinema where cinema=:cinema", Movie.class)
                .setParameter("cinema", m)
                .getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public Movie save(Movie c) {
        entityManager.persist(c);
        return c;
    }
}