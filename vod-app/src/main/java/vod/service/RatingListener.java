package vod.service;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import vod.model.Movie;
import vod.repository.MovieDao;

@Component
@RequiredArgsConstructor
@Slf4j
public class RatingListener {

    private final MovieDao movieDao;

    @KafkaListener(
            topics = "ratings-topic",
            groupId = "vod-group",
            properties = {"spring.json.value.default.type=vod.service.RatingListener.RatingEvent"})
    void listenRatings(RatingEvent event){
        log.info("rating event: {}", event);
        Movie movie = movieDao.findById(event.movieId).orElseThrow();

        movie.setRating(event.averageRating);

        movieDao.save(movie);
    }

    @Data
    public static class RatingEvent{
        private int movieId;
        private double averageRating;
    }

}
