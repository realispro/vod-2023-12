package vod;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import vod.model.Cinema;
import vod.model.Movie;
import vod.model.Rating;
import vod.service.CinemaService;
import vod.service.MovieService;

import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class VodRunner implements CommandLineRunner {

    private final MovieService movieService;
    private final CinemaService cinemaService;
    @Override
    public void run(String... args) throws Exception {

        List<Cinema> cinemas = cinemaService.getAllCinemas();
        log.info(cinemas.size() + " cinemas found:");
        cinemas.forEach(System.out::println);

        Movie movie = movieService.getMovieById(2);
        List<Rating> ratings = movieService.getRatingsByMovie(movie);
        log.info("Rating of movie {}", movie.getTitle());
        ratings.forEach(rating -> log.info("rate: {}", rating.getRate()));


    }
}
