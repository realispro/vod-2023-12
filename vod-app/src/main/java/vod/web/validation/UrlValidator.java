package vod.web.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.net.MalformedURLException;
import java.net.URL;

public class UrlValidator implements ConstraintValidator<Url, String> {
    @Override
    public boolean isValid(String uriString, ConstraintValidatorContext constraintValidatorContext) {
        try {
            new URL(uriString);
            return true;
        } catch (MalformedURLException e) {
            return false;
        }
    }
}
