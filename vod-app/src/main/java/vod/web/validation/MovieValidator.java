package vod.web.validation;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import vod.service.MovieService;
import vod.web.rest.dto.MovieDTO;

@Component
@RequiredArgsConstructor
public class MovieValidator implements Validator {

    private final MovieService movieService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(MovieDTO.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MovieDTO movieDTO = (MovieDTO) target;

        if(movieService.getDirectorById(movieDTO.getDirectorId())==null){
            errors.rejectValue(
                    "directorId",
                    "errors.director.missing",
                    new Object[]{movieDTO.getDirectorId()},
                    "Missing director " + movieDTO.getDirectorId());
        }
    }
}
