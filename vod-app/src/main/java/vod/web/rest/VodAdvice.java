package vod.web.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import vod.web.validation.MovieValidator;

@RestControllerAdvice(
        //basePackages = "vod.web.rest",
        annotations = Adviced.class
)
@RequiredArgsConstructor
@Slf4j
public class VodAdvice {

    private final MovieValidator movieValidator;

    @InitBinder("movieDTO")
    void initBinder(WebDataBinder binder){
        binder.addValidators(movieValidator);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException e){
        log.error("illegal argument exception", e);
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(e.getMessage());
    }

    //@ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    void handleIllegalArgumentException(Exception e){
        log.error("exception", e);
    }
}
