package vod.web.rest;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import vod.model.Movie;
import vod.service.MovieService;
import vod.web.rest.dto.MovieDTO;

import java.net.URI;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/webapi")
@Adviced
public class MovieRest {

	private final MovieService movieService;
	private final MessageSource messageSource;
	private final LocaleResolver localeResolver;

	@GetMapping(value = "/movies") // /movies?pageNumber=2&pageSize=10
	@ResponseStatus(HttpStatus.OK)
	List<MovieDTO> getMovies(
			@RequestParam(value = "pageNumber", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", required = false) Integer pageSize,
			@RequestHeader(value = "foo-header", required = false) String fooHeader,
			@RequestHeader Map<String, String> headers,
			@CookieValue(value = "vod-cookie", required = false) String vodCookie) {

		log.info("about to retrieve movies");
		log.info("pageNumber: {}, pageSize: {}", pageNumber, pageSize);
		log.info("foo-header: {}", fooHeader);

		headers.entrySet().stream()
						.filter(entry->entry.getKey().startsWith("foo-"))
						.forEach(entry->log.info("foo- header: {}, {}", entry.getKey(), entry.getValue()));

		log.info("vod-cookie: {}", vodCookie);

		List<Movie> allMovies = movieService.getAllMovies();
		return allMovies.stream()
				.map(MovieDTO::toDTO)
				.toList();
	}

	@GetMapping(value = "/movies/{id}")
	ResponseEntity<MovieDTO> getMovie(@PathVariable("id") int id) {
		log.info("about to retrieve single movie");
		Movie movie = movieService.getMovieById(id);
		if (movie != null) {
			return ResponseEntity.ok(MovieDTO.toDTO(movie));
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PostMapping("/movies")
	ResponseEntity<?> addMovie(@Validated @RequestBody MovieDTO movieDTO, Errors errors, HttpServletRequest request){
		log.info("about to add movie: {}", movieDTO);

		if(errors.hasErrors()){

			Locale locale = localeResolver.resolveLocale(request);
					//new Locale("fr", "FR");
			// messages_fr_FR
			// messages_fr
			// messages_<OS_LANG>
			// messages

			List<String> errorMessages = errors.getAllErrors().stream()
					.map(error->messageSource.getMessage(error.getCode(), error.getArguments(), locale))
					.toList();

			return ResponseEntity.badRequest().body(errorMessages);
		}

		if(movieDTO.getTitle().equals("Boom!")){
			throw new IllegalArgumentException("Boom!");
		}

		Movie movie = movieService.addMovie(movieDTO.toData());

		URI uri = ServletUriComponentsBuilder
				.fromCurrentRequestUri()
				.path("/{id}")
				.build(Map.of("id", movie.getId()));

		return ResponseEntity
				.created(uri)
				.body(MovieDTO.toDTO(movie));
	}



}
