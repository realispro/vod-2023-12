package vod.web.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vod.model.Cinema;
import vod.service.CinemaService;
import vod.web.rest.dto.CinemaDTO;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/webapi")
@Adviced
public class CinemaRest {

    private final CinemaService cinemaService;

    @GetMapping(value = "/cinemas")
    List<CinemaDTO> getCinemas(){
        log.info("about to retrieve cinemas");
        List<Cinema> cinemas = cinemaService.getAllCinemas();

        return cinemas.stream()
                .map(CinemaDTO::toDTO)
                .toList();
    }

    @GetMapping("/cinemas/{cinemaId}")
    ResponseEntity<CinemaDTO> getCinema(@PathVariable("cinemaId") int cinemaId){
        log.info("about to retrieve cinema {}", cinemaId);
        Cinema cinema = cinemaService.getCinemaById(cinemaId);
        if(cinema!=null) {
            return ResponseEntity
                    .status(200)
                    .body(CinemaDTO.toDTO(cinema));
        }else {
            return ResponseEntity
                    .status(404)
                    .build();
        }
    }

}
