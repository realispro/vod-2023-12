package vod.web.rest.dto;

import lombok.Data;
import vod.model.Cinema;

@Data
public class CinemaDTO {

    private int id;
    private String name;
    private String logo;

    public static CinemaDTO toDTO(Cinema data){
        CinemaDTO dto = new CinemaDTO();
        dto.setId(data.getId());
        dto.setName(data.getName());
        dto.setLogo(data.getLogo());
        return dto;
    }
}
