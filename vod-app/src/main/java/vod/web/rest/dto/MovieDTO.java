package vod.web.rest.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import vod.model.Director;
import vod.model.Movie;
import vod.web.validation.Url;

@Data
public class MovieDTO {

    private int id;
    @NotNull
    @Size(min=1, max = 256)
    private String title;
    @NotNull
    @Url
    private String poster;
    private int directorId;

    public static MovieDTO toDTO(Movie data){
        MovieDTO dto = new MovieDTO();
        dto.setId(data.getId());
        dto.setTitle(data.getTitle());
        dto.setPoster(data.getPoster());
        dto.setDirectorId(data.getDirector().getId());
        return dto;
    }

    public Movie toData(){
        Movie data = new Movie();
        data.setId(this.id);
        data.setTitle(this.title);
        data.setPoster(this.poster);
        Director director = new Director();
        director.setId(this.directorId);
        data.setDirector(director);
        return data;
    }

}
