package rating.mongo;

import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;
import rating.model.Rating;

@Component
@RequiredArgsConstructor
public class RatingDocumentListener extends AbstractMongoEventListener<Rating> {

    private final MongoSequenceService sequenceService;

    @Override
    public void onBeforeConvert(final BeforeConvertEvent<Rating> event) {
        if (event.getSource().getId() == null) {
            event.getSource().setId(sequenceService.getNextValue());
        }
    }

}