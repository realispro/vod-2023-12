package rating.service;


import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import rating.model.Rating;
import rating.repository.RatingRepository;

import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

@Service
@RequiredArgsConstructor
@Slf4j
@EnableAsync
public class RatingNotifier {

    private final RatingRepository ratingRepository;
    private final KafkaTemplate kafkaTemplate;

    @Async
    Future<Double> notifyAverageRating(int movieId){
        double averageRating = ratingRepository.findAllByMovieId(movieId).stream()
                .mapToDouble(Rating::getRate)
                .average()
                .orElse(0);

        RatingEvent event = new RatingEvent(movieId, averageRating);

        try {
            Thread.sleep(10_000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        kafkaTemplate.send("ratings-topic", event);

        return new AsyncResult(averageRating);

    }

    @Value
    public static class RatingEvent{
        private final int movieId;
        private final double averageRating;
    }

    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(100);
        executor.setMaxPoolSize(200);
        executor.setQueueCapacity(500);
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        return executor;
    }
}
