package rating.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import rating.model.Rating;

import java.util.List;

public interface RatingRepository extends MongoRepository<Rating, Integer> {

    List<Rating> findAllByMovieId(int movieId);
}
