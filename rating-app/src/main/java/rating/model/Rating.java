package rating.model;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Rating {

    private Integer id;
    @Min(1)
    @Max(5)
    private float rate;
    private int movieId;
}
