package spring;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import spring.config.Air;
import spring.config.ExecutionTime;
import spring.event.TravelFinishedEvent;
import spring.event.TravelStarterEvent;

@Component
@Scope("singleton")
@Lazy
public class Travel {

    private final ApplicationEventPublisher publisher;
    private final String name;
    private final Transportation transportation;
    private final Accomodation accomodation;

    public Travel(
            ApplicationEventPublisher publisher,
            String name,
            @Air(fast = false) Transportation transportation,
            Accomodation accomodation) {
        this.publisher = publisher;
        this.transportation = transportation;
        this.accomodation = accomodation;
        this.name = name;
        System.out.println("constructing travel object using parametrized constructor...");
    }

    @ExecutionTime
    public void travel(Person p){
        System.out.println("started travel '" + name + "' for a person " + p);
        publisher.publishEvent(new TravelStarterEvent(this, p));
        transportation.transport(p);
        accomodation.host(p);
        transportation.transport(p);
        publisher.publishEvent(new TravelFinishedEvent(this, p));
    }

    @Override
    public String toString() {
        return "Travel{" +
                "name='" + name + '\'' +
                ", transportation=" + transportation +
                ", accomodation=" + accomodation +
                '}';
    }
}
