package spring.impl;


import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import spring.Accomodation;
import spring.Person;

import java.util.ArrayList;
import java.util.List;

@Component
@PropertySource("classpath:/meals.properties")
public class Hotel implements Accomodation {

    @Value("#{'${meals.gratis:polo-kokta}'.toUpperCase()}")
    private String gratis;

    private List<String> meals;

    @Autowired
    public void xyz(
            //@Qualifier("meals")
            @Value("#{'${meals.menu}'.split(',')}") List<String> meals) {
        this.meals = new ArrayList<>(meals);
        this.meals.add(gratis);
    }

    @PostConstruct
    public void postConstruct() throws Exception {
        System.out.println("hotel properties set: " + this.meals);
    }

    @PreDestroy
    public void destroy() throws Exception {
        System.out.println("hotel destroyed!");
    }

    @Override
    public void host(Person p) {
        System.out.println("person " + p + " is being hosted in hotel. meal: " + meals);
    }



}
