package spring.event;

import spring.Person;

public class TravelStarterEvent extends TravelEvent{
    public TravelStarterEvent(Object source, Person person) {
        super(source, person);
    }
}
