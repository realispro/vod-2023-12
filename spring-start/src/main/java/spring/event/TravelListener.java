package spring.event;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class TravelListener implements ApplicationListener<TravelEvent> {
    @Override
    public void onApplicationEvent(TravelEvent event) {
        System.out.println("[TRAVEL EVENT] received: " + event);
    }
}
