package spring.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import spring.Person;
import spring.TravelException;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;

@Component
@Aspect
public class TravelAspect {

    //private static final String ALL_IN_IMPL = "execution(public * spring.impl..*(..))";

    @Pointcut("execution(public * spring.impl..*(..))")
    void allInImpl(){}


    @Pointcut("execution(public * *(spring.Person))")
    void allAcceptingPerson(){}

    @Before("allInImpl()")
    void logEnteringMethod(JoinPoint jp){
        System.out.println("[***ENTER***] entering method: " + jp.getSignature().getName()
                + " on " + jp.getTarget().getClass().getSimpleName());
    }

    @After("allInImpl()")
    void logExitingMethod(JoinPoint jp){
        System.out.println("[***EXIT***] exiting method: " + jp.getSignature().getName()
                + " on " + jp.getTarget().getClass().getSimpleName());
    }

    @Around("@annotation(spring.config.ExecutionTime) || allAcceptingPerson()")
    Object measureExecutionTime(ProceedingJoinPoint jp) throws Throwable {

        Instant start = Instant.now();
        Object result = jp.proceed(jp.getArgs());
        Instant end = Instant.now();
        System.out.println("[***TIME***] method: " + jp.getSignature().getName()
                + " execution took " + Duration.between(start, end) );

        return result;
    }

    @Before("allInImpl()")
    void checkValidateTicket(JoinPoint jp) throws Throwable {
        var person = (Person) jp.getArgs()[0];
        LocalDate valid = person.getTicket().getValid();
        if (person.getTicket() != null && LocalDate.now().isAfter(valid)) {
            throw new RuntimeException("Panie " + person.getLastName() + " Pana bilet jest nieaktualny");
        }
    }

    @AfterThrowing(value = "allAcceptingPerson()", throwing = "ex")
    void repackageToTravelException(RuntimeException ex){
        throw new TravelException("repackaged exception", ex);
    }

}
