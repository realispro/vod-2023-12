package spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.util.List;

@Configuration
@ComponentScan("spring")
@EnableAspectJAutoProxy
public class TravelConfig {

    @Bean
    String travelName(){
        return "Summer holiday 2024";
    }

    @Bean("meals")
    List<String> meals(){
        return List.of(
               "coca-cola",
               "hamburger"
        );
    }


}
